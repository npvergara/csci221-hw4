#include <iostream>
using namespace std;

struct node{
	int data;
	struct node* next;
};

void Push(struct node** headRef, int dataValue){
	struct node* newNode = new struct node;
	newNode->next = *headRef;
	newNode->data = dataValue;
	*headRef = newNode;
}

void Print(struct node* head){
	if(head == NULL) return;
	cout << head->data << " ";
	Print(head->next);
}

void PrintWithLoop(struct node* head){
	while(head != NULL){
		cout << head->data << " ";
		head = head->next;
	}
}

void reverse(struct node** headRef){
	struct node* newlist = NULL;
	struct node* current = *headRef;
	struct node* next;

	while(current != NULL){
		next = current->next;
		current->next = newlist;
		newlist = current;
		current = next;
	}
	*headRef = newlist;
}

int Length(struct node* head){
	int count = 0;
	while(head != NULL){
		count = count + 1;
		head = head->next;
	}
	return count;
}

void AddToEnd(struct node** headRef, int dataValue){
	struct node* current = *headRef;
	if(current == NULL){
		Push(headRef, dataValue);
	}else{
		while(current->next != NULL){
			current = current->next;
		}
		Push(&(current->next), dataValue);
	}

}

void RemoveNode(struct node** headRef, int dataValue){
	struct node* current = *headRef;
	struct node* prev = NULL;

	while(current!= NULL){

		if(current->data == dataValue){
			if(current == *headRef){
				(*headRef)=(*headRef)->next;
			}else{
				prev->next = current->next;
			}
			delete current;
			return;
		}
		prev = current;
		current = current->next;

	}

	delete current;

}

void InsertSorted(struct node** headRef, int dataValue){
	struct node* newNode = new struct node;
	newNode->data = dataValue;
	struct node* current = *headRef;

	if(*headRef == NULL || dataValue <= (*headRef)->data ){
		newNode->next = (*headRef);
		(*headRef) = newNode;
	}else{
		while( (current->next !=NULL) && current->next->data <= newNode->data  ){
			current = current->next;
		}
		newNode->next = current->next;
		current->next = newNode;
	}
}

//Code for the HW
//Question 1
struct node* findToLast(struct node** headRef, int toLast){
 struct node* result = *headRef;
 //Doesn't work on an empty list
 if(*headRef != NULL){
  //Makes sure the list is big enough
  if(toLast > Length(*headRef)){
   cout << "Linked list is too small" << endl;
  }
  else{
   //Making the pointers
   struct node* point1 = *headRef;
   struct node* point2 = *headRef;
   //Moves the second pointer steps ahead of the first one
   for(int i = 0; i < toLast; i++){
    point2 = point2->next;
   }
   //Moves both pointers until pointer2 reaches the end
   while(point2 != NULL){
    point1 = point1->next;
    point2 = point2->next;
   }
   //Gets the result
   result = point1;
   cout << "Result's value: " << result->data << endl;
   delete point1;
   delete point2;
  }
 }
 return result;
}
//Question 2
void remDuplicates(struct node* head){
 //Creates the two pointers I need
 struct node* mainPointer = head;
 struct node* runner = mainPointer;
 //Iteration of mainPointer
 while(mainPointer != NULL){
  //Iterates the runner through the list, checking for duplicates
  while((runner->next) != NULL){
   //Looks for a duplicate
   if((runner->next)->data == mainPointer->data){
    //Stores the duplicate, gets rid of it, and moves the runner along
    struct node* duplicate = runner->next;
    runner->next = (runner->next)->next;
    delete duplicate;
   }
   else{
    runner = runner->next;
   }
  }
  mainPointer = mainPointer->next;
  runner = mainPointer;
 }
 delete mainPointer;
 delete runner;
}

int main() {
 struct node* head = NULL;

// Push(&head, 70);
// Push(&head, 60);
// Push(&head, 50);
// Push(&head, 40);
// Push(&head, 20);
// Print(head);
// cout << endl;

// cout << "3rd to last entry's address: " << findToLast(&head, 3) << endl;

 Push(&head, 9);
 Push(&head, 50);
 Push(&head, 42);
 Push(&head, 19);
 Push(&head, 50);
 Push(&head, 42);
 Print(head);
 cout << endl;
 remDuplicates(head);
 Print(head);
 cout << endl;

 return 0;
}
